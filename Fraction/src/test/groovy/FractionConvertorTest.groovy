import spock.lang.Specification

import java.util.concurrent.LinkedTransferQueue

import static TestUtils.uid

/**
 * Created by rmasgutov on 09.10.2015.
 */
class FractionConvertorTest extends Specification {

    def "Should return period of string"() {
        expect:
        period == FractionConvertor.isPeriodical(string)
        where:
        period | string
        '123'  | '123123'
        ''     | '123'
        '056'  | '123056056'
        '456'  | '0.123432456456'
        '4'    | '44'
    }

    def "Convert integer part to notation 2 should return expected number"() {
        given:
        def number = new BigDecimal("22")

        when:
        def result = FractionConvertor.convertIntegerPartToNotation(number, 2)

        then:
        result == '10110'
    }

    def "Convert integer part to notation 8 should return expected number"() {
        given:
        def number = new BigDecimal("456")

        when:
        def result = FractionConvertor.convertIntegerPartToNotation(number, 8);

        then:
        result == '710'
    }

    def "Convert integer part to notation 16 should return expected number"() {
        given:
        def number = new BigDecimal("456")

        when:
        def result = FractionConvertor.convertIntegerPartToNotation(number, 16)

        then:
        result == '1C8'
    }

    def "Convert fractional part to notation 2 should return expected number"() {
        given:
        def number = new BigDecimal("0.22")
        FractionConvertor.periodSize = FractionConvertor.periodSize / (2 / 10);

        when:
        def result = FractionConvertor.convertFractionalPart(number, 2)

        then:
        result == '0(01110000101000111101)'
    }

    def "Convert fractional part to notation 8 should return expected number"() {
        given:
        def number = new BigDecimal("0.22")

        FractionConvertor.periodSize = FractionConvertor.periodSize / (8 / 10);
        when:
        def result = FractionConvertor.convertFractionalPart(number, 8)

        then:
        result == '1(60507534121727024365)'
    }

    def "Convert fractional part to notation 16 should return expected number"() {
        given:
        def number = new BigDecimal("0.22")
        FractionConvertor.periodSize = 5;

        when:
        def result = FractionConvertor.convertFractionalPart(number, 16)

        then:
        result == '3(851EB)'
    }

    def "Convert fractional part with period to notation 16 should return expected number"() {
        given:
        def number = new BigDecimal(((Double)(1/12d)).toString())

        when:
        def result = FractionConvertor.convertFractionalPart(number, 16)

        then:
        result == '1(5)'
    }

    def "Convert fraction to notation 16 should return expected number"() {
        given:
        Fraction fraction = newFraction(4801, 12, 16)

        when:
        FractionConvertor.convert(fraction)

        then:
        fraction.toString() == '190.1(5)'
    }

    Fraction newFraction(int numerator, int divider, int notation) {
        Fraction fraction = new Fraction()
        fraction.setNumerator((double) numerator)
        fraction.setDivider((double) divider)
        fraction.setScaleOfNotation(notation)
        fraction
    }

    Queue<Integer> newQueueWithDifferentNumbers() {
        Queue<Integer> queue = new LinkedTransferQueue<>()
        for (int i = 0; i < 3; i++) {
            queue.add(uid());
        }
        queue
    }

    Queue<Integer> newQueueWithSameNumber(int number) {
        Queue<Integer> queue = new LinkedTransferQueue<>()
        for (int i = 0; i < 3; i++) {
            queue.add(number);
        }
        queue
    }
}
