import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Класс для конвертации дроби в произвольную систему счисления.
 * Возможен перевод в любую систему счисления до 16 включительно
 *
 * Created by rmasgutov on 07.10.2015.
 */
public class FractionConvertor {
    /**
     * Алфавит символов для перевода.
     */
    private static String[] numbers = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};

    /**
     * Размер периодической последовательности символов.
     */
    static int periodSize = 10;

    /**
     * Точность деления.
     */
    static int scaleDivide = 100;

    /**
     * Определение наличие периода в строке символов.
     *
     * @param string строка.
     * @return период.
     */
    static String isPeriodical(String string) {
        String period = "";
        int stringLength = string.length();
        for (int i = 0; i < stringLength / 2; i++) {
            String str1 = string.substring(stringLength - i - 1, stringLength);
            String str2 = string.substring(stringLength - 2 * (i + 1), stringLength - i - 1);
            if (str1.equals(str2)) {
                period = string.substring(stringLength - i - 1, stringLength);
            }
        }

        return period;
    }

    /**
     * Конвертация целой части числа.
     *
     * @param number число.
     * @param scaleInt  система счисления.
     * @return переведенной число.
     */
    static String convertIntegerPartToNotation(BigDecimal number, Integer scaleInt) {
        StringBuilder result = new StringBuilder();
        BigDecimal scale = new BigDecimal(scaleInt.toString());
        do {
            result.append(numbers[number.remainder(scale).intValue()]);
            number = number.divide(scale, BigDecimal.ROUND_FLOOR);

        } while (!number.equals(BigDecimal.ZERO));
        return result.reverse().toString();
    }

    /**
     * Конвертация дробной части числа.
     *
     * @param modulo дробная часть.
     * @param scaleInt  система счисления.
     * @return переведенной число.
     */
    static String convertFractionalPart(BigDecimal modulo, Integer scaleInt) {
        StringBuilder result = new StringBuilder();
        BigDecimal scale = new BigDecimal(scaleInt.toString());
        String period;
        do {
            modulo = modulo.multiply(scale);
            result.append(numbers[modulo.setScale(0, RoundingMode.FLOOR).intValue()]);
            period = isPeriodical(result.toString());
            modulo = modulo.remainder(BigDecimal.ONE);
        } while ((period.equals("") || period.length() < periodSize) && !modulo.equals(BigDecimal.ZERO));
        if (!period.equals("")) {
            result.delete(result.indexOf(period + period), result.length());
            String tempPeriod = period;
            String newPeriod;
            do {
                newPeriod = tempPeriod;
                tempPeriod = isPeriodical(newPeriod);
            } while (!tempPeriod.equals(""));
            result.append("(").append(newPeriod).append(")");
        }
        return result.toString();
    }

    /**
     * Перевод дроби в систему счисления.
     *
     * @param fraction дробь.
     */
    public static void convert(Fraction fraction) throws Exception {
        if (fraction.getDivider() == 0) {
            throw new Exception("Деление на ноль");
        }
        if (fraction.getScaleOfNotation() < 10) {
            periodSize = periodSize / (fraction.getScaleOfNotation() / 10);
        } else {
            periodSize = 5;
        }
        BigDecimal num = new BigDecimal(((Double)fraction.getNumerator()).toString());
        BigDecimal div = new BigDecimal(((Double)fraction.getDivider()).toString());
        BigDecimal tempResult =  num.divide(div, scaleDivide, RoundingMode.HALF_UP);
        BigDecimal reminder = tempResult.remainder(BigDecimal.ONE);
        BigDecimal scaled = tempResult.setScale(0,RoundingMode.FLOOR);

        fraction.setIntegerPart(convertIntegerPartToNotation(scaled, fraction.getScaleOfNotation()));
        fraction.setFractionPart(convertFractionalPart(reminder, fraction.getScaleOfNotation()));
    }
}
