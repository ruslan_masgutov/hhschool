import java.util.Scanner;

/**
 * Created by rmasgutov on 09.10.2015.
 */
public class KeyboardInput implements Input {
    Scanner scanner = new Scanner(System.in);

    @Override
    public Point nextPoint() {
        String s;
        do {
            System.out.println("Do you want add new point?(y/n)");
            s = scanner.nextLine();
        } while (!s.equals("y") && !s.equals("n"));
        if (s.equals("y")) {
            System.out.println("Please write coordinates for point( example: \"3 4\")");
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            scanner.nextLine();
            return new Point(x, y);
        } else {
            return null;
        }
    }

    @Override
    public Fraction nextFraction() {
        String s;
        do {
            System.out.println("Do you want add new fraction?(y/n)");
            s = scanner.nextLine();
        } while (!s.equals("y") && !s.equals("n"));
        if (s.equals("y")) {
            System.out.println("Please write numerator, divider and scale( example: \"3 4 16\")");
            double numerator = scanner.nextDouble();
            double divider = scanner.nextDouble();
            int scale = scanner.nextInt();
            scanner.nextLine();
            return new Fraction(numerator, divider, scale);
        } else {
            return null;
        }
    }
}
