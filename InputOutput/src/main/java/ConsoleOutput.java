/**
 * Created by rmasgutov on 09.10.2015.
 */
public class ConsoleOutput implements Output {

    @Override
    public void outString(Object value) {
        System.out.println(value);
    }
}
