/**
 * Интерфейс для вывода данных.
 * Created by rmasgutov on 09.10.2015.
 */
public interface Output {
    /**
     * Закрытие потока вывода.
     *
     * @throws Exception
     */
    default void close() throws Exception {

    }

    /**
     * Вывод объекта.
     *
     * @param value объект.
     * @throws Exception
     */
    void outString(Object value) throws Exception;
}
