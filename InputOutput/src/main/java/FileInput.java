import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Created by rmasgutov on 09.10.2015.
 */
public class FileInput implements Input {

    private InputStream inputStream;

    private Scanner scanner;

    public FileInput(String filePath) throws Exception {
        inputStream = new BufferedInputStream(new FileInputStream(filePath));
        scanner = new Scanner(inputStream);
    }

    @Override
    public void close() throws Exception {
        inputStream.close();
    }

    @Override
    public Point nextPoint() {
        if (scanner.hasNextInt()) {
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            return new Point(x, y);
        } else {
            return null;
        }
    }

    @Override
    public Fraction nextFraction() {
        if (scanner.hasNextInt()) {
            double numerator = scanner.nextDouble();
            double divider = scanner.nextDouble();
            int scale = scanner.nextInt();
            return new Fraction(numerator, divider, scale);
        } else {
            return null;
        }
    }
}
