import java.io.File;
import java.io.FileWriter;

/**
 * Created by rmasgutov on 09.10.2015.
 */
public class FileOutput implements Output {
    private FileWriter fileWriter;

    public FileOutput(String filePath) throws Exception {
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        fileWriter = new FileWriter(file, true);

    }

    @Override
    public void outString(Object value) throws Exception {
        fileWriter.write(value.toString() + "\n");
    }

    @Override
    public void close() throws Exception {
        fileWriter.close();
    }
}
