import java.util.ArrayList;
import java.util.List;

/**
 * Интерфейс ввода данных.
 * Created by rmasgutov on 09.10.2015.
 */
public interface Input {

    /**
     * Метод закрытия потока ввода.
     *
     * @throws Exception
     */
    default void close() throws Exception {

    }

    /**
     * Получение сущности точки.
     *
     * @return Point.
     */
    Point nextPoint();

    /**
     * Получение сущости дроби.
     *
     * @return Fraction.
     */
    Fraction nextFraction();

    /**
     * Получение всех точек из входных данных.
     *
     * @return список точек.
     */
    default List<Point> getPoints() {
        List<Point> list = new ArrayList<>();
        Point point;
        do {
            point = nextPoint();
            list.add(point);
        } while (point != null);
        list.remove(list.size() - 1);
        return list;
    }

    /**
     * Получение всех дробей из взодных данных.
     *
     * @return список дробей.
     */
    default List<Fraction> getFractions() {
        List<Fraction> list = new ArrayList<>();
        Fraction fraction;
        do {
            fraction = nextFraction();
            list.add(fraction);
        } while (fraction != null);
        list.remove(list.size() - 1);
        return list;
    }

}
