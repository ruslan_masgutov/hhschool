import java.util.Date;
import java.util.Random;

/**
 * Created by rmasgutov on 06.10.2015.
 */
public class TestUtils {
    private static Random generator = new Random(new Date().getTime());
    private static int maxValue = 1000;

    public static int uid() {
        return generator.nextInt(maxValue);
    }
}
