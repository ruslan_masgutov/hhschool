import java.util.Arrays;
import java.util.List;

/**
 * Точка входа.
 * Аргументы:
 * task1 - задача "Минимальное расстояние"
 * task2 - задача "Дробь"
 * -fin - указание пути до файла с данными(при отсутствии параметра используется клавиатурный ввод)
 * -fout - указание пути до файла для вывода(при отсутствии параметра используется вывод в консоль)
 * <p>
 * Пример запуска Solve task1 -fin "1.txt" -fout "2.txt"
 * <p>
 * Created by rmasgutov on 09.10.2015.
 */
public class Solve {
    public static void main(String[] args) throws Exception {
        List<String> arg = Arrays.asList(args);
        String fin = "";
        String fout = "";
        for (int i = 1; i < arg.size() - 1; i++) {
            if (arg.get(i).equals("-fin")) {
                fin = arg.get(i + 1);
            }
            if (arg.get(i).equals("-fout")) {
                fout = arg.get(i + 1);
            }
        }

        Input input;
        if (!fin.equals("")) {
            input = new FileInput(fin);
        } else {
            input = new KeyboardInput();
        }
        Output output;
        if (!fout.equals("")) {
            output = new FileOutput(fout);
        } else {
            output = new ConsoleOutput();
        }

        if (arg.contains("task1")) {
            List<Point> points = input.getPoints();
            DefinitionClosestDistance task1 = new DefinitionClosestDistance(points);
            Double result = task1.getSolve();
            output.outString(result);
        }
        if (arg.contains("task2")) {
            List<Fraction> fractions = input.getFractions();
            for (Fraction fraction : fractions) {
                FractionConvertor.convert(fraction);
                output.outString(fraction);
            }
        }

        output.close();
        input.close();
    }
}
