import lombok.*;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Класс для определения минимального расстояния в наборе точек.
 * Created by rmasgutov on 05.10.2015.
 */
@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DefinitionClosestDistance {
    private List<Point> points;

    /**
     * Определение расстояния между двумя точками.
     *
     * @param p1 1 точка.
     * @param p2 2 точка.
     * @return расстояние.
     */
    double calculateDistance(Point p1, Point p2) {
        Point temp;
        if (p1.compareTo(p2) > 0) {
            temp = p2;
            p2 = p1;
            p1 = temp;
        }
        return Math.sqrt(Math.pow(p2.getX() - p1.getX(), 2)
                + Math.pow(p2.getY() - p1.getY(), 2));
    }

    /**
     * Определения наиболее короткого расстояния из набора точек.
     *
     * @param points набор точек.
     * @return минимальное расстояние.
     */
    double getClosestDistance(List<Point> points) {
        double minDistance = calculateDistance(points.get(0), points.get(1));
        for (int i = 0; i < points.size(); i++) {
            for (int j = i + 1; j < points.size(); j++) {
                minDistance = Math.min(minDistance, calculateDistance(points.get(i), points.get(j)));
            }
        }
        return minDistance;
    }

    /**
     * Найти минимальное расстояние между точками в опредеденной окрестности.
     *
     * @param middle   точка.
     * @param distance расстояние.
     * @return минимальное расстояние.
     */
    double findAroundMiddle(Point middle, double distance) {
        List<Point> previewList = points
                .stream()
                .filter(point -> (middle.getX() < point.getX() ?
                        point.getX() - middle.getX() :
                        middle.getX() - point.getX()) < distance)
                .sorted(Point::compareToByY)
                .collect(Collectors.toList());
        if (previewList.size() > 1) {
            double minDistance = calculateDistance(previewList.get(0), previewList.get(1));
            for (int i = 2; i < previewList.size(); i++) {
                for (int j = 1; j < i; j++) {
                    minDistance = Math.min(minDistance, calculateDistance(previewList.get(j), previewList.get(i)));
                }
            }
            return minDistance;
        }
        return distance;

    }

    /**
     * Определения минимального расстояния из набора точек.
     *
     * @param points список точек.
     * @return минимальное расстояние.
     */
    double findMinDistanceInPointList(List<Point> points) {
        if (points.size() > 3) {
            int middleIndex = points.size() % 2 == 0 ? points.size() / 2 : points.size() / 2 + 1;

            double left = findMinDistanceInPointList(points.subList(0, middleIndex));
            double right = findMinDistanceInPointList(points.subList(middleIndex, points.size()));
            double candidate = Math.min(left, right);

            return Math.min(candidate, findAroundMiddle(points.get(middleIndex - 1), candidate));

        } else {
            return getClosestDistance(points);
        }
    }

    /**
     * Получить решение для заданного набора точек.
     *
     * @return минимальное расстояние.
     */
    public double getSolve() throws Exception {
        if (points.size() < 2) {
            throw new Exception("В наборе меньше 2 точек");
        }
        Collections.sort(points);

        return findMinDistanceInPointList(points);
    }

}
