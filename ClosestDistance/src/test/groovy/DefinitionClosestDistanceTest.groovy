import Point
import spock.lang.Specification

import static TestUtils.uid

/**
 * Created by rmasgutov on 06.10.2015.
 */
class DefinitionClosestDistanceTest extends Specification {
    def process = new DefinitionClosestDistance()

    def "Calculate distance between two points"() {
        given:
        List<Point> pointList = [new Point(1, 1), new Point(1, 5)]

        when:
        def result = process.calculateDistance(pointList.get(0), pointList.get(1))

        then:
        result == 4d

    }

    def "Calculate distance between two points in revert order"() {
        given:
        List<Point> pointList = [new Point(5, 5), new Point(1, 5)]

        when:
        def result = process.calculateDistance(pointList.get(0), pointList.get(1))

        then:
        result == 4d

    }

    def "Calculate minimal distance in small group points"() {
        given:
        List<Point> pointList = getPointsListSorted(3);
        def expected = getExpectedMinimalDistance(pointList)
        when:
        def result = process.getClosestDistance(pointList)

        then:
        result == expected

    }

    def "Calculate minimal distance in large group points"() {
        given:
        List<Point> pointList = getPointsList(100);
        def expectedResult = getExpectedMinimalDistance(pointList)
        process.setPoints(pointList)

        when:
        def result = process.getSolve()

        then:
        result == expectedResult
    }

    List<Point> getPointsListSorted(int count) {
        List<Point> pointList = getPointsList(count)
        pointList.sort { a, b -> a <=> b }
        println pointList
        pointList
    }

    List<Point> getPointsList(int count) {
        List<Point> pointList = new ArrayList<>(count)
        for (int i = 0; i < count; i++) {
            pointList.add(new Point(uid(), uid()))
        }
        pointList
    }


    def getExpectedMinimalDistance(List<Point> points) {
        Point p1 = points.get(0)
        Point p2 = points.get(1)

        double minDistance = process.calculateDistance(p1, p2);
        double tempDistance
        for (int i = 0; i < points.size(); i++) {
            for (int j = 0; j < points.size(); j++) {
                if (i != j) {
                    tempDistance = process.calculateDistance(points.get(i), points.get(j))
                    minDistance = Math.min(minDistance, tempDistance)
                    if (tempDistance == minDistance) {
                        p1 = points.get(i)
                        p2 = points.get(j)
                    }
                }
            }
        }
        println "p1 $p1"
        println "p2 $p2"
        println "minDistance $minDistance"
        minDistance
    }

}
