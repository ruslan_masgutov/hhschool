import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Сущность точки.
 * Created by rmasgutov on 05.10.2015.
 */

@ToString
@Getter
@Setter
public class Point implements Comparable<Point> {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int compareTo(Point point) {
        if (this.x == point.x) {
            return this.y < point.y ? -1 : 1;
        }
        return this.x < point.x ? -1 : 1;
    }

    public int compareToByY(Point point) {
        if (this.y == point.y) {
            return this.x < point.x ? -1 : 1;
        }
        return this.y < point.y ? -1 : 1;
    }
}
