import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Сущность дроби.
 * Created by rmasgutov on 07.10.2015.
 */
@Getter
@Setter
@NoArgsConstructor
public class Fraction {
    private double numerator;
    private double divider;
    private int scaleOfNotation = 10;
    private String integerPart;
    private String fractionPart;

    Fraction(double numerator, double divider, int scaleOfNotation) {
        this.numerator = numerator;
        this.divider = divider;
        this.scaleOfNotation = scaleOfNotation;
    }

    @Override
    public String toString() {
        return integerPart + "." + fractionPart;
    }
}
