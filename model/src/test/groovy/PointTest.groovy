import spock.lang.Specification

/**
 * Created by rmasgutov on 06.10.2015.
 */
class PointTest extends Specification {
    def "Compare to points by X axis should return p1 large than p2"() {
        given:
        def p1 = new Point(2, 3)
        def p2 = new Point(1, 3)

        when:
        def result = p1.compareTo(p2)

        then:
        result == 1

    }

    def "Compare to points by X axis should return p2 large than p1"() {
        given:
        def p1 = new Point(1, 3)
        def p2 = new Point(2, 3)

        when:
        def result = p1.compareTo(p2)

        then:
        result == -1

    }

    def "Compare to points by Y axis should return p1 large than p2"() {
        given:
        def p1 = new Point(2, 3)
        def p2 = new Point(1, 2)

        when:
        def result = p1.compareToByY(p2)

        then:
        result == 1

    }

    def "Compare to points by Y axis should return p2 large than p1"() {
        given:
        def p1 = new Point(2, 2)
        def p2 = new Point(1, 3)

        when:
        def result = p1.compareToByY(p2)

        then:
        result == -1

    }

    def "Compare to points with equals X coordinate by X axis should return p1 large than p2"() {
        given:
        def p1 = new Point(2, 3)
        def p2 = new Point(2, 2)
        when:
        def result = p1.compareTo(p2)

        then:
        result == 1

    }
}
